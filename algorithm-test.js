// -   create function to determine the given expression is correct or not
// -   expression will be string of open bracket nor close bracket e.g `({[[()]]})`
// -   your function should determine the expression is it correct or not

// [][][] // true
// ()[][{}] // true
// [[{[}]]] // false

function bracket(data) {
    
    
    const dataBracket = data.split("")

    for(let i = 0; i < dataBracket.length; i++ ) {
        if(dataBracket[i] == "(") {
            if(dataBracket[i+1] == ")") {
                return true
            }
        }
        else if (dataBracket[i] == "{") {
            if (dataBracket[i+1] == "}") {
                return true
            }
        }
        else if (dataBracket[i] == "[") {
            if (dataBracket[i+1] == "]") {
                return true
            }
        }
        else {
            return false
        }
    }

}

console.log(bracket("[][][]"))
console.log(bracket("[[{[}]]]"))
console.log(bracket("([]}}}"))